<!DOCTYPE html>
<html>
<?php
require_once 'head.php';
?>
    <body>

        <div class="container">

            <div class="container-fluid">
                <section class="plano">
                    
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <nav class="navbar menu ">

                                <a class="navbar-brand nome" href="index.php">
                                    <img src="img/images.png" width="35" height="35" class="d-inline-block align-top" alt="Valhöll">
                                    Valhöll Administrador
                                </a>

                            </nav>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-9 col-sm-8 col-md-5 col-lg-12 col-xl-4">
                            <hr>
                            <label class="texto">Digite a senha e seu nome de usuario:</label>
                        </div>

                    </div>

                    <form action="/store.php" method="POST" >

                        <div class="form-group">
                            <div class="row">
                                <div class="col-9 col-sm-8 col-md-5 col-lg-12 col-xl-4">
                                    <label class="texto">Nome </label>
                                    <input class="tela"  type="text" name="usuario">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-9 col-sm-8 col-md-5 col-lg-5 col-xl-4">
                                    <label class="texto">Senha </label>
                                    <input class="tela" type="password" name="senha">
                                </div>
                            </div>
                        </div>


                        <div class="form-group">

                            <div class="row">
                                <div class="col-9 col-sm-8 col-md-5 col-lg-5 col-xl-5">
                                    <button class="login" type="submit">Entrar</button>
                                </div>
                            </div>
                        </div>

                    </form>


                </section>
            </div>
        </div>
<?php
require_once 'footer.php';
?>

    </body>

</html>
<html>
<?php
require_once 'head.php';
?>

<body>
    <section class="plano">
        <div class="container">
            <div class="row>"
                 <div class="col-12 col-sm-12  col-lg-form-12  col-xl-12">
            <nav class="navbar menu ">

                <a class="navbar-brand nome" href="index.php.php">
                    <img src="img/images.png" width="35" height="35" class="d-inline-block align-top" alt="Valhöll">
                    Valhöll Exclusão de produtos.
                </a>

            </nav>
                </div>
        </div>
                <?php
                require_once 'produtos.php';
                ?>
            
            <div class="row">
                <div class="col-12 col-sm-12  col-lg-form-12  col-xl-12">
                    <label class="texto text-center"> Preencha o ID produto para excluir:</label>
                </div>
            </div>
            <br>
            <div class="row justify-content-center">
                <div class="col col-sm-12 col-md-6">
                    <form action="Controller/ProdutoController.php" method="POST" class="form">
                        <input type="hidden" name="acao" value="excluir">
                        
                        <div class="form-group">
                            <input type="number" class="form-control" name="ID" min="1">
                        </div>
                        
                        <input class="botão" type="submit" value="Deletar">
                    </form>

                </div>

            </div>
        </div>
        </section>
    <?php
require_once 'footer.php';
?>
</body>

</html>



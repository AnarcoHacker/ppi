<!DOCTYPE html>
<html>

    <?php
    require_once 'head.php';
    ?>
    

     
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
        <img class="d-block w-100" src="img/carrosel1.jpg"  width="800" height="400" alt="Primeiro Slide">
    <div class="carousel-caption d-none d-md-block">
           <h3 class="legc">Venha e confira nossos preços</h3>
    <p class="legc">Tudo em cultura nordica, veja nossas promoções para esse mês</p>
       </div>
    </div>
      
    <div class="carousel-item">
        <img class="d-block w-100" src="img/carrosel2.jpg" width="800" height="400" alt="Segundo Slide">
       <div class="carousel-caption d-none d-md-block">
           <h3 class="legc">Na compra de 50 reais em produtos, concorra a uma passagem para Irlanda</h3>
    <p class="legc">Promoção disponivel por tempo inderterminado</p>
       </div>
    </div>
    <div class="carousel-item">
        <img class="d-block w-100" src="img/carrosel3.jpg"  width="800" height="400" alt="Terceiro Slide">
     <div class="carousel-caption d-none d-md-block">
    <h3 class="legc">Joias e semi-joias em prata, aço e inox</h3>
    <p class="legc">Modelos masculino e feminino</p>
       </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Anterior</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Próximo</span>
  </a>
</div>
 
    
</html>
<?php
ini_set('display_errors', 1);
require_once 'Dao/ProdutoDao.php';



$produtoDao = new ProdutoDao();

$produtos = $produtoDao->listarProdutos();
?>
<!DOCTYPE html>
<html>
<?php
require_once 'head.php';
?>

<body>
    

   
    <div class="container">
        <div class="row">
            <?php foreach ($produtos as $produto) { ?>
                <div class=" col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <img src="<?= $produto->linki ?>" width="150" height="150"/><br>
                    <p class="produto">ID: <?= $produto->id ?><br></p>
                    <p class="produto">Nome: <?= $produto->nome ?><br></p>
                    <p class="produto">Preço:<?= $produto->preco ?><br></p>
                    <p class="produto">Quantidade: <?= $produto->quantidade ?></p>
                    <button class="botão" type="submit">Comprar</button>
                    <br>
                    <br>
                </div>
            <?php } ?>
        </div>
    </div>
</body>

</html>
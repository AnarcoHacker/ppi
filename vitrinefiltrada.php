<?php
ini_set('display_errors', 1);
require_once 'Dao/ProdutoDao.php';

$tipo=$_REQUEST['tipo'];

$produtoDao = new ProdutoDao();

$produtos = $produtoDao->buscar($tipo);

?>
<!DOCTYPE html>
<html>
<?php
require_once 'head.php';
?>

<body>
    

   
    <div class="container">
        <div class="row">
            <?php foreach ($produtos as $produto) { ?>
                <div class=" col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                     <form action="comprar.php" method="POST" class="form">
                        <div class="circle">
                        
                        <img src="<?= $produto->linki ?>" width="150" height="150"/><br>
                        </div>
                    <p class="produto"> <?= $produto->nome ?><br></p>
                    <p class="produto">Preço: <?= $produto->preco ?><br></p>
                    <p class="produto">Quantidade: <?= $produto->quantidade ?></p>
                    <button class="botão" name='id' value="<?= $produto->id ?>" type="submit">Comprar</button>
                    </form>
                    <br>
                    <br>
                </div>
            <?php } ?>
        </div>
    </div>
</body>

</html>
<html>
<?php
require_once 'head.php';
?>

<body>
    <section class="plano">
        <div class="container">

            <nav class="navbar menu ">

                <a class="navbar-brand nome" href="index.php">
                    <img src="img/images.png" width="35" height="35" class="d-inline-block align-top" alt="Valhöll">
                    Valhöll Cadastro de produtos.
                </a>

            </nav>
            <div class="row">
                <div class="col-12 col-sm-12  col-lg-form-12  col-xl-12">
                    <label class="texto text-center"> Preencha os dados abaixo para cadastrar o produto:</label>
                </div>
            </div>
            <br>
            <div class="row justify-content-center">
                <div class="col col-sm-12 col-md-6">
                    <form action="Controller/ProdutoController.php" method="POST" class="form">
                        <input type="hidden" name="acao" value="salvar">
                        
                        <div class="form-group">
                            <input type="text" class="form-control" name="nome" placeholder="Nome do produto">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="preco" placeholder="Preço em dólar">
                        </div>
                        <div class="form-group">
                            <input type="number" class="form-control" name="quantidade" min="1">
                        </div>
                         <div class="form-group">
                            <input type="text" class="form-control" name="linki" placeholder="link da imagem">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="tipo" placeholder="tipo">
                        </div>
                        <input class="botão" type="submit" value="Cadastrar">
                    </form>

                </div>

            </div>
        </div>
        </section>
    <?php
require_once 'footer.php';
?>
</body>

</html>
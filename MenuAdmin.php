<!DOCTYPE html>

<html>
    
<?php

require_once 'head.php';

?>
    
    
<body>
    
    
    <div class="container-fluid">
        
        <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <nav class="navbar menu ">

                        <a class="navbar-brand nome" href="/site.php">
                            <img src="img/images.png" width="35" height="35" class="d-inline-block align-top" alt="Valhöll">
                            Valhöll Administrador
                        </a>

                    </nav>
                </div>
            </div>

        <div class="row">
            <div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                <form action="form_produto.php" method="POST" class="form">

                    <button class="admin" type="submit">Cadastrar</button>

                </form>
            </div>

            <div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                <form action="alterar.php" method="POST" class="form">

                    <button class="admin" type="submit">Alterar</button>

                </form>
            </div>

            <div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                <form action="deleta.php" method="POST" class="form">

                    <button class="admin" type="submit">Excluir</button>

                </form>
            </div>
        </div>
    </div>
    
</body>
</html>

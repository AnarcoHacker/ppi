<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/Util/Conexao.php"; // statico
// require_once '../Util/Conexao.php';



class ProdutoDao
{

    public function __construct()
    {
        
    }

    public function salvar($produto)
    {
        try {
            $sql = 'insert into produtos (nome, preco, quantidade,linki,tipo)
         values (:nome, :preco, :quantidade , :linki, :tipo)';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':nome', $produto['nome']);
            $p_sql->bindValue(':preco', $produto['preco']);
            $p_sql->bindValue(':quantidade', $produto['quantidade']);
            $p_sql->bindValue(':linki', $produto['linki']);
            $p_sql->bindValue(':tipo', $produto['tipo']);
            $p_sql->execute();
            echo 'sucesso';
            return;
          
        } catch (Exception $e) {
            print_r($e);
        }
    }
    
    public function excluir($ID){
        
        try{
            
            $sql = 'DELETE FROM produtos WHERE id = :ID';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':ID', $ID);
            $p_sql->execute();
            echo 'sucesso';
            return;
        } catch (Exception $e) {
            print_r($e);
        }
    }
    
    public function editar($produto){
        
        try{
            
            $sql = 'update produtos set nome = :nome, preco = :preco, quantidade = :quantidade, linki= :linki, tipo = :tipo where id = :id';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':id', $produto['id']);
            $p_sql->bindValue(':nome', $produto['nome']);
            $p_sql->bindValue(':preco', $produto['preco']);
            $p_sql->bindValue(':quantidade', $produto['quantidade']);
            $p_sql->bindValue(':linki', $produto['linki']);
            $p_sql->bindValue(':tipo', $produto['tipo']);
            $p_sql->execute();
            echo 'sucesso';
        } catch (Exception $e) {
             print_r($e);
        }
        
    }

    public function listarProdutos(){
        try {
            $sql = 'select * from produtos';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            print_r($e);
        }
    }
    
    
    public function buscar($tipo){
        
        try {
            $sql = 'SELECT * FROM produtos WHERE tipo = :tipo';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':tipo', $tipo);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            print_r($e);
        }
    }
    
    public function comprar($tipo){
        
        try {
            $sql = 'SELECT * FROM produtos WHERE id = :id';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':id', $tipo);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            print_r($e);
        }
    }
}


 
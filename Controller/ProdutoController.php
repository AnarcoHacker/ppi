<?php

ini_set('display_errors', 1);

require_once '../Dao/ProdutoDao.php';

$produtoDao = new ProdutoDao();

switch ($_REQUEST['acao']) {
    
    case 'salvar':
        $produto = [
            'nome' => strtoupper($_REQUEST['nome']),
            'preco' => $_REQUEST['preco'],
            'quantidade' => $_REQUEST['quantidade'],
            'linki' => $_REQUEST['linki'],
            'tipo' => $_REQUEST['tipo'],
        ];
        $produtoDao->salvar($produto);
        break;
    
    case 'excluir':
        
        $ID = $_REQUEST['ID'];
        $produtoDao->excluir($ID);
        break;
    
    case 'editar':
        
       $produto = [
    
            'id'=>strtoupper($_REQUEST['id']),
            'nome' => $_REQUEST['nome'],
            'preco' => $_REQUEST['preco'],
            'quantidade' => $_REQUEST['quantidade'],
            'linki' => $_REQUEST['linki'],
            'tipo' => $_REQUEST['tipo'],
    
        ];
        $produtoDao->editar($produto);
        break;
    
    
    default:
        echo 'ação não conhecida';
}
